//
//  main.m
//  WebviewCrash
//
//  Created by Rasmus Styrk on 05/04/15.
//  Copyright (c) 2015 House of Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
