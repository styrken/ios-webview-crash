//
//  AppDelegate.h
//  WebviewCrash
//
//  Created by Rasmus Styrk on 05/04/15.
//  Copyright (c) 2015 House of Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

